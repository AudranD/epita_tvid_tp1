install:
	pip install ./lib --user

clean:
	rm -rf ~/.local/lib/python3.8/site-packages/tvid* ./build/ ./dist/ ./tvid.egg-info/ ./target/ ./lib/tvid/tvid*
