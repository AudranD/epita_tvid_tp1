# EPITA/IMAGE: TP Video 1 

The answers to the questions of the TP are in the the notebook `notebook.ipynb`.

> If you don't want to run the notebook, an offline version is available: `notebook.html`.

## Authors
- Audran Doublet
- Sami Issaadi

## Setup

1. Install [**Rust**](https://rustup.rs/)
2. Run the following commands:
```sh
# Setup the rust lib
pip install -r ./lib/requirements.txt
make install

# Open the notebook
jupyter notebook
```

> The main methods have been written in **Rust** for performances concern. Hence,
the setup might can take few seconds.

# Uninstall

```sh
make clean
```
