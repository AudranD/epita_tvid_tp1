extern crate pyo3;

use crate::utils;

use ndarray::*;
use rayon::prelude::*;

pub enum TypeBMA {
    Classic,
    WithRegularization{ weighting: f32 },
}

pub struct BMA {
    bma_type: TypeBMA,

    radius: i64,
    min_error: i64,

    frame_0: Array2<i16>,
    frame_1: Array2<i16>,

    block_width: usize,
    block_height: usize,

    block_count_x: i64,
    block_count_y: i64,
}

impl BMA {
    /**
     * Init a new BMA object
     * @param frame_0 the initial frame
     * @param frame_1 the final frame
     * @param block_size translation block size
     */
    pub fn new(bma_type: TypeBMA, frame_0: Array2<i16>, frame_1: Array2<i16>, block_size: (usize, usize)) -> BMA {
        let (shape_y, shape_x) = (frame_1.shape()[0], frame_1.shape()[1]);
        let (block_width, block_height) = block_size;

        BMA {
            bma_type,

            frame_0,
            frame_1,

            block_width,
            block_height,

            block_count_x: ((shape_x + block_width - 1) / block_width) as i64,
            block_count_y: ((shape_y + block_height - 1) / block_height) as i64,

            radius: 10,
            min_error: (block_width * block_height * 3) as i64,
        }
    }

    /**
     * change the radius of the BMA search
     */
    pub fn radius(&mut self, radius: i64) -> &mut BMA {
        self.radius = radius;
        self
    }

    /**
     * change the minimal error of the BMA search (we keep the first x such that loss(x) < min_error)
     */
    pub fn min_error(&mut self, min_error: i64) -> &mut BMA {
        self.min_error = min_error;
        self
    }

    /**
     * retrieve a view on a block of frame0/frame1
     */
    fn block_slice<'a>(&'a self, frame: &'a Array2<i16>, x: i64, y: i64) -> ArrayView2<i16> {
        let x = self.block_width * x as usize;
        let y = self.block_height * y as usize;

        let x_end = (x + self.block_width).min(frame.shape()[1]);
        let y_end = (y + self.block_height).min(frame.shape()[0]);

        frame.slice(s![y..y_end, x..x_end])
    }

    /**
     * retrieve a mutable view on a block of frame0/frame1
     */
    fn mut_block_slice<'a>(&self, frame: &'a mut Array2<i16>, x: i64, y: i64) -> ArrayViewMut2<'a, i16> {
        let x = self.block_width * x as usize;
        let y = self.block_height * y as usize;

        let x_end = (x + self.block_width).min(frame.shape()[1]);
        let y_end = (y + self.block_height).min(frame.shape()[0]);

        frame.slice_mut(s![y..y_end, x..x_end])
    }

    /**
     * compute simple loss
     */
    fn simple_loss(&self, y: i64, x: i64, y_new: i64, x_new: i64) -> Option<i64> {
        let slice_0 = self.block_slice(&self.frame_0, x, y);
        let slice_1 = self.block_slice(&self.frame_1, x_new, y_new);

        if slice_0.shape() != slice_1.shape() {
            None
        } else {
            Some((&slice_0 - &slice_1).mapv(|x| x.abs() as i64).sum())
        }
    }

    /**
     * compute distance regularization
     */
    fn regularization(&self, weighting: f32, dx: i64, dy: i64) -> i64 {
        (weighting + dx.pow(2) as f32 + dy.pow(2) as f32).round() as i64
    }

    /**
     * compute loss (with or without regularization)
     */
    fn loss(&self, y: i64, x: i64, y_new: i64, x_new: i64) -> Option<i64> {
        let base = self.simple_loss(y, x, y_new, x_new);

        if let Some(base) = base {
            Some(match &self.bma_type {
                &TypeBMA::Classic  => base,
                &TypeBMA::WithRegularization {weighting} => base + self.regularization(weighting, x - x_new, y - y_new),
            })
        } else {
            None
        }
    }

    /**
     * check if block coordinates are valid
     */
    fn valid_block_xy(&self, &(y, x): &(i64, i64)) -> bool {
        x >= 0 && y >= 0 && x < self.block_count_x && y < self.block_count_y
    }

    /**
     * Run the BMA algorithm
     * @return A move vector for each blocks
     */
    pub fn estimate_motion(&self) -> ArrayD<i16> {
        let offsets = utils::generate_all_offsets(self.radius);

        ArrayD::from_shape_vec(IxDyn(&[self.block_count_y as usize, self.block_count_x as usize, 2]), 
            (0..self.block_count_x*self.block_count_y)
                .into_par_iter()
                .flat_map(|i| {
                    let x = i % self.block_count_x;
                    let y = i / self.block_count_x;

                    let mut min = std::i64::MAX;
                    let mut coords = (0, 0);

                    let curr_offsets = offsets.iter()
                                              .map(|(dy, dx)| (y+dy, x+dx))
                                              .filter(|v| self.valid_block_xy(v));

                    // Find minimal loss between neighbouring blocks
                    // Stop if we find a loss < min_error (faster but not as accurate)
                    for (y_new, x_new) in curr_offsets {
                        if let Some(loss) = self.loss(y, x, y_new, x_new) {
                            if loss < min {
                                min = loss;
                                coords = (y_new, x_new);

                                if min < self.min_error {
                                    break;
                                }
                            }
                        }
                    }

                    vec![(coords.0 - y) as i16, (coords.1 - x) as i16]
               }).collect()
        ).unwrap()
    }

    /**
     * reconstruct frame1 from the frame0 one and precomputed motion vectors
     * @param motion motion vectors
     */
    pub fn reconstruct(&self, motion: ArrayViewD<i16>) -> ArrayD<i16> {
        let mut result = Array2::zeros((self.frame_1.shape()[0], self.frame_1.shape()[1]));

        for y in 0..self.block_count_y {
            for x in 0..self.block_count_x {
                let (dy, dx) = (motion[[y as usize, x as usize, 0]], motion[[y as usize, x as usize, 1]]);

                // This should only be useful when the motion vector is compressed
                let vx = (x + dx as i64).min(self.block_count_x - 1).max(0);
                let vy = (y + dy as i64).min(self.block_count_y - 1).max(0);

                self.mut_block_slice(&mut result, x, y).assign(
                    &self.block_slice(&self.frame_0, vx, vy)
                );
            }
        }

        ArrayD::from_shape_vec(
            IxDyn(&[self.frame_1.shape()[0], self.frame_1.shape()[1]]), 
            result.into_raw_vec(),
        ).unwrap()
    }
}
