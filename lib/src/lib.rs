pub mod utils;
pub mod bma;

use bma::*;

use ndarray::Array2;

use numpy::{IntoPyArray, PyArrayDyn, PyReadonlyArrayDyn};

use pyo3::prelude::*;
use pyo3::exceptions;

#[pymodule]
/**
 * python bindings
 */
fn tvid(_py: Python<'_>, m: &PyModule) -> PyResult<()> {
    // wrapper for bma
    #[pyfn(m, "estimate_motion")]
    /**
     * Estimate the motion between frame0 and frame1
     * @param frame_0 the reference frame
     * @param frame_1 the moved frame
     * @param block_dims dimension of the block to be moved
     * @param radius (optional) maximal search radius for blocks
     * @param min_error (optional) accepted error for block search
     * @param regularization (optional) weighting of the regularization (no regularization if the parameter isn't specified)
     * @return motion vectors
     */
    pub fn estimate_motion<'py>(py: Python<'py>,
                    frame_0: PyReadonlyArrayDyn<i16>,
                    frame_1: PyReadonlyArrayDyn<i16>,
                    block_dims: (usize, usize),
                    radius: Option<i64>,
                    min_error: Option<i64>,
                    regularization: Option<f32>) -> PyResult<&'py PyArrayDyn<i16>>
    {
            let bma_type = match regularization {
                Some(weighting) => TypeBMA::WithRegularization { weighting },
                _               => TypeBMA::Classic,
            };

            if frame_0.shape().len() != 2 {
                return Err(exceptions::PyValueError::new_err("frame_0 should be a 2d array"));
            }
            if frame_1.shape().len() != 2 {
                return Err(exceptions::PyValueError::new_err("frame_1 should be a 2d array"));
            }

            let frame_0 = Array2::from_shape_vec(
                (frame_0.shape()[0], frame_0.shape()[1]),
                frame_0.to_vec()?
            ).unwrap();

            let frame_1 = Array2::from_shape_vec(
                (frame_1.shape()[0], frame_1.shape()[1]),
                frame_1.to_vec()?
            ).expect("value error");

            let mut r = BMA::new(
                bma_type,
                frame_0,
                frame_1,
                block_dims,
            );

            if let Some(min_error) = min_error {
                r.min_error(min_error);
            }
            if let Some(radius) = radius {
                r.radius(radius);
            }

            Ok(r.estimate_motion().into_pyarray(py))
    }

    #[pyfn(m, "reconstruct")]
    /**
     * reconstruct an image from a reference frame and a precomputed motion
     * @param frame_0 the reference frame
     * @param motion motion vectors
     * @param block_dims dimension of the block to be moved
     * @return the reconstructed frame
     */
    pub fn reconstruct<'py>(py: Python<'py>,
                    frame_0: PyReadonlyArrayDyn<i16>,
                    motion: PyReadonlyArrayDyn<i16>,
                    block_dims: (usize, usize)
                ) -> PyResult<&'py PyArrayDyn<i16>>
    {
            if frame_0.shape().len() != 2 {
                return Err(exceptions::PyValueError::new_err("frame_0 should be a 2d array"));
            }
            if motion.shape().len() != 3 {
                return Err(exceptions::PyValueError::new_err("motion should be a 3d array"));
            }

            let frame_0 = Array2::from_shape_vec(
                (frame_0.shape()[0], frame_0.shape()[1]),
                frame_0.to_vec()?
            ).unwrap();

            let r = BMA::new(
                TypeBMA::Classic,
                frame_0.clone(),
                frame_0,
                block_dims,
            );

            Ok(r.reconstruct(motion.as_array()).into_pyarray(py))
    }

    Ok(())
}

#[cfg(test)]
/*
 * simple rust tests
 */
mod tests {
    use super::*;

    use std::error::Error;
    use std::path::Path;

    macro_rules! timeit {
        ($name:expr, $code:expr) => ({
            use std::time::Instant;

            let t0 = Instant::now();
            let result = $code;
            let t1 = Instant::now();

            print!("{}: ", $name);
            let time = t1.checked_duration_since(t0).unwrap().as_millis();

            if time < 1000 {
                println!("{}ms", time);
            } else {
                println!("{}s", time as f32 / 1000.);
            }

            result
        })
    }

    #[test]
    fn bma1x1() -> Result<(), Box<dyn Error>> {
        let f0 = utils::load_gray_image(&Path::new("data/img_1.png"))?;
        let f1 = utils::load_gray_image(&Path::new("data/img_2.png"))?;

        let bma1x1 = BMA::new(TypeBMA::Classic, f0, f1, (1, 1));
        timeit!("BMA 1x1", bma1x1.estimate_motion());

        Ok(())
    }

    #[test]
    fn bma10x10() -> Result<(), Box<dyn Error>> {
        let f0 = utils::load_gray_image(&Path::new("data/img_1.png"))?;
        let f1 = utils::load_gray_image(&Path::new("data/img_2.png"))?;

        let bma10x10 = BMA::new(TypeBMA::Classic, f0, f1, (10, 10));
        timeit!("BMA 10x10", bma10x10.estimate_motion());

        Ok(())
    }

}
