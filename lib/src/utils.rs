use itertools::*;

use std::path::Path;
use std::error::Error;

use ndarray::Array2;

/**
 * Load an image and convert it to grayscale
 * @return a ndarray or an IO error
 */
pub fn load_gray_image(path: &Path) -> Result<Array2<i16>, Box<dyn Error>> {
    let image = image::open(path)?.to_luma();

    Ok(
        Array2::from_shape_vec(
            (image.height() as usize, image.width() as usize),
            image.into_raw(),
        )?.into_owned().mapv(|e| e as i16)
    )
}

/**
 * Generate all (dx, dy) offset for a specified radius
 * @param radius
 * @return a vector of offsets
 */
pub fn generate_all_offsets(radius: i64) -> Vec<(i64, i64)> {
    iproduct!((-radius..=radius), (-radius..=radius))
        .sorted_by_key(|(y,x)| y.abs() + x.abs())
        .collect_vec()
}
